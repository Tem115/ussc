﻿
namespace TestUSSC.Models
{
    // Класс игральной карты.
    public class Card
    {
        public string Value { get; } // Ценность(значение) карты.
        public char Suit { get; }  // Масть карты.
        public Card(string value, char suit)
        {
            Value = value;
            Suit = suit;
        }
    }
}
