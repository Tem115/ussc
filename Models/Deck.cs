﻿using System;
using System.Linq;

namespace TestUSSC.Models
{
    // Класс колоды карт.
    public class Deck
    {
        private static readonly char[] suits = { '♥', '♦', '♣', '♠' }; // Массив карточных мастей.
        private static readonly string[] values = { "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K" }; // Массив значений карт.
        
        // Создание колоды карт.
        public Card[] Cards { get; } = (from suit in suits
                                        from value in values
                                        select new Card(value, suit)).ToArray();
        
        // Метод перетасовки колоды карт.
        public void ShuffleDeck()
        {
            for (int index = Cards.Length - 1; index >= 1; index--)
            {
                Random random = new Random();
                int replacementIndex = random.Next(index + 1);
                var temp = Cards[replacementIndex];
                Cards[replacementIndex] = Cards[index];
                Cards[index] = temp;
            }
        }
    }
}
