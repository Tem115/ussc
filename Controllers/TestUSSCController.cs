﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using TestUSSC.Models;


namespace TestUSSC.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TestUSSCController : ControllerBase
    {
        private static Dictionary<string, Deck> Decks = new Dictionary<string, Deck>();

        //Метод получения всех колод.
        [HttpGet("Main")]
        public IActionResult GetDecks()
        {
            return new ObjectResult(Decks);
        }

        //Метод получения колоды по имени.
        [HttpGet("Deck")]
        public IActionResult GetDeck(string name)
        {
            return CheckContainKey(name, () => new ObjectResult(Decks[name]));
        }

        //Метод получения списка названий колод.
        [HttpGet("DeckNames")]
        public IActionResult GetDeckNames()
        {
            return new ObjectResult(Decks.Keys);
        }

        //Метод создания колоды.
        [HttpPost("CreateDeck")]
        public IActionResult CreateDeck(string name)
        {
            return CheckCreateDeck(name, () => Decks.Add(name, new Deck()));
        }

        //Метод удаления колоды по имени.
        [HttpDelete("DeleteDeck")]
        public IActionResult DeleteDeck(string name)
        {
            return CheckContainKey(name, () => { Decks.Remove(name);return Ok(); });
        }

        //Метод перетасовки колоды по имени.
        [HttpGet("ShuffleDeck")]
        public IActionResult ShuffleDeck(string name)
        {
            return CheckContainKey(name, () => { Decks[name].ShuffleDeck(); return Ok(); });
        }



        //Метод проверки наличия названия колоды в колодах.
        private IActionResult CheckContainKey(string name, Func<IActionResult> action)
        {
            if (Decks.ContainsKey(name))
            {
                return action();
            }
            else
            {
                ModelState.AddModelError("deck name", "Колоды с данным именем не существует");
                return BadRequest(ModelState);
            }
        }

        //Метод проверки отсутствия названия колоды в колодах.
        private IActionResult CheckCreateDeck(string name,Action action)
        {
            if (!Decks.ContainsKey(name))
            {
                action();
                return Ok();
            }
            else
            {
                ModelState.AddModelError("deck name", "Данное название колоды уже используется");
                return BadRequest(ModelState);
            }
        }
    }
}
